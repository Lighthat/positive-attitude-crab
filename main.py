#!/bin/python3
import discord
from discord.ext import commands
import random
import asyncio
import requests
from dataclasses import dataclass
from html import unescape

description = '''An example bot to showcase the discord.ext.commands extension
module.
There are a number of utility commands being showcased here.'''

@dataclass
class User:
    name: str
    money: int

@dataclass
class Question:
    question: str
    answer: str
    choices: dict
    difficulty: str
    category: str

client = discord.Client()
users = {}
flag = False

def scrambleList(orig):
    dest = orig[:]
    random.shuffle(dest)
    return dest

def getTriviaQuestion():
    difficulty = random.randint(1,3)
    questionUrl = 'https://opentdb.com/api.php?amount=1&difficulty=medium&type=multiple'
    if difficulty == 1:
        questionUrl = 'https://opentdb.com/api.php?amount=1&difficulty=easy&type=multiple'
    elif difficulty == 2:
        questionUrl = 'https://opentdb.com/api.php?amount=1&difficulty=medium&type=multiple'
    else:
        questionUrl = 'https://opentdb.com/api.php?amount=1&difficulty=hard&type=multiple'
    question = requests.get(url=questionUrl).json()['results'][0]
    category = question['category']
    difficulty = question['difficulty']
    questionText = unescape(question['question'])
    answer = unescape(question['correct_answer'])
    choices = question['incorrect_answers']
    choices.append(answer)
    decodedChoices = []
    for choice in choices:
        decodedChoices.append(unescape(choice))

    answerId = 0
    letters = ['a', 'b', 'c', 'd']
    choiceList = scrambleList(decodedChoices)
    for idx,choice in enumerate(choiceList):
        if choice == answer:
            answerId = idx
    answerLetter = letters[answerId]
    choiceDict = {}
    for idx,choice in enumerate(choiceList):
        choiceDict[letters[idx]] = choice

    

    return Question(question=questionText, answer=answerLetter, choices=choiceDict, difficulty=difficulty, category=category)

def getGcgWinner(rolls: dict):
    key_max = max(rolls.keys(), key=(lambda k: rolls[k]))
    key_min = min(rolls.keys(), key=(lambda k: rolls[k]))
    return(key_max, key_min)

def addUserToUserList(user: str, money: int = 0):
    if user not in users:
        newUser = User(name=user, money=int(money))
        users[user] = newUser
    print('adding user ' + user + ' with money ' + str(money))

def readMoneyFile():
    global users
    file = open('juicebox', 'r')
    readUsers = file.read().split('\n')
    for user in readUsers:
        data = user.split(';;')
        if data[0] != '':
            addUserToUserList(data[0], data[1])
    print('file has been read, users are: ' + str(users))

def saveMoneyFile():
    filestring = ''
    for k,v in users.items():
        filestring = filestring + v.name + ';;' + str(v.money) + '\n'

    print('saving')
    file = open('juicebox', 'w')
    file.write(filestring)
    file.close()
    print('saved')

def addMoney(user: str, amount: int):
    print('giving '+str(amount)+'mL to ' + user)
    if user not in users:
        addUserToUserList(user, amount)
    else:
        users[user].money += amount
    saveMoneyFile()

def subtractMoney(user: str, amount: int):
    print('taking '+str(amount)+'mL from ' + user)
    if user not in users:
        addUserToUserList(user, 0)
    else:
        users[user].money -= amount
    if users[user].money < 0:
        users[user].money = 0
    saveMoneyFile()

class MyContext(commands.Context):
    async def tick(self):
        emoji = '\N{WHITE HEAVY CHECK MARK}'
        try:
            # this will react to the command author's message
            await self.message.add_reaction(emoji)
        except discord.HTTPException:
            pass

class MyBot(commands.Bot):
    async def get_context(self, message, *, cls=MyContext):
        # when you override this method, you pass your new Context
        # subclass to the super() method, which tells the bot to
        # use the new MyContext class
        return await super().get_context(message, cls=cls)

bot = MyBot(command_prefix='?', description=description)
genderList = ["agender", "androgyne", "androgynous", "bigender", "cis", "cisgender", "cis-female", "cis-male", "cis-man", "cis-woman", "cisgender-female", "cisgender-male", "cisgender-man", "cisgender-woman", "female-to-male", "ftm", "gender-fluid", "gender-nonconforming", "gender-questioning", "gender-variant", "genderqueer", "intersex", "male-to-female", "mtf", "neither", "neutrois", "non-binary", "other", "pangender", "trans", "trans", "trans-female", "trans-female", "trans-male", "trans-male", "trans-man", "trans-man", "trans-person", "trans-person", "trans-woman", "trans-woman", "transfeminine", "transgender", "transgender-female", "transgender-male", "transgender-man", "transgender-person", "transgender-woman", "transmasculine", "transsexual", "transsexual-female", "transsexual-male", "transsexual-man", "transsexual-person", "transsexual-woman", "two-spirit"]
currencyString = 'mL of lemon juice'
# GCG Variables
gcgPlayers = []
currentlyPlayingGcg = False
gcgGameAmount = 0

# Trivia Variables
currentlyPlayingTrivia = False
triviaReward = 0
triviaAnswer = ''
triviaAnswers = {}

@bot.event
async def on_message(message):
    global flag
    if message.author.id == bot.user.id:
        return
    
    maxJuice = 150
    number = random.randint(1, 30)
    juice = random.randint(1,maxJuice)
    exclamation = ''
    emoji1 = '💵'
    emoji2 = '😝'
    emoji3 = '💦'
    emoji4 = '🥜'

    person = message.author.name
    channel = message.channel

    if(number == 2):
        if juice == maxJuice:
            await message.add_reaction(emoji1)
            await message.add_reaction(emoji2)
            await message.add_reaction(emoji3)
            await message.add_reaction(emoji4)
            exclamation = ':FeelsAmazingMan: NUT'
        elif juice / maxJuice >= 0.9:
            await message.add_reaction(emoji1)
            await message.add_reaction(emoji2)
            await message.add_reaction(emoji3)
            exclamation = ':tongue: :sweat_drops:'
        elif juice / maxJuice >= 0.75:
            exclamation = 'Juicy!'
        elif juice / maxJuice >= 0.5:
            exclamation = 'Sweet.'
        elif juice / maxJuice >= 0.25:
            exclamation = 'Slurp'
        elif juice == 1:
            exclamation = 'Get fucked retard :dab:'
        else:
            exclamation = '_drip_'
        addMoney(str(message.author), juice)
        await channel.send(str(person) + ' just got ' + str(juice) + currencyString + '. ' + exclamation)
        await message.add_reaction('\N{MONEY BAG}')

    if 'lemon' in message.content:
        await message.add_reaction('\N{LEMON}')
    await bot.process_commands(message)

@bot.event
async def on_ready():
    readMoneyFile()
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')

@bot.command()
async def roll(ctx, dice: str):
    """Rolls a dice in NdN format."""
    try:
        rolls, limit = map(int, dice.split('d'))
    except Exception:
        await ctx.send('Format has to be in NdN!')
        return

    result = ', '.join(str(random.randint(1, limit)) for r in range(rolls))
    await ctx.send(result)

@bot.command(description='For when you wanna settle the score some other way')
async def choose(ctx, *choices: str):
    """Chooses between multiple choices."""
    await ctx.send(random.choice(choices))

@bot.command()
async def joined(ctx, member: discord.Member):
    """Says when a member joined."""
    await ctx.send('{0.name} joined in {0.joined_at}'.format(member))

@bot.command()
async def gcgstart(ctx, number: int=0):
    """ Starts a GCG game """
    global currentlyPlayingGcg
    global gcgGameAmount
    global currencyString
    if number == 0:
        await ctx.send('You need to start a game with more than 0'+currencyString)
        return
    try:
        if currentlyPlayingGcg:
            return
        else:
            currentlyPlayingGcg = True
            gcgGameAmount = number
            await ctx.send('Welcome to the 💲Lucky🍋Lemon💲. Type ?gcgplay to get into the game. Type ?gcgroll to start the rolls.')
    except Exception:
        print('Failed')
        return

@bot.command()
async def gcgstop(ctx):
    """ Stops a GCG game """
    global currentlyPlayingGcg
    global gcgPlayers
    currentlyPlayingGcg = False
    gcgPlayers = []
    gcgGameAmount = 0
    await ctx.send('We are not gambling anymore.')

@bot.command()
async def gcgplay(ctx):
    """ Joins a GCG game """
    global currentlyPlayingGcg
    global gcgPlayers
    global users
    global gcgGameAmount

    if currentlyPlayingGcg:
        user = str(ctx.author)
        if user not in users:
            await ctx.send('You have no money, ' + str(user))
            return 
        else:
            if users[user].money < gcgGameAmount:
                await ctx.send('You don\'t have enough money, ' + str(user))
                return
        player = ctx.author
        if ctx.author not in gcgPlayers:
            gcgPlayers.append(player)
            await ctx.tick()
    else:
        await ctx.send('We are not currently gambling.')

@bot.command()
async def gcgroll(ctx):
    """ Rolls for all people in the current GCG game. """
    global currentlyPlayingGcg
    global gcgPlayers
    global gcgGameAmount

    if currentlyPlayingGcg:

        # Make sure the person starting the rolls is in the game
        inGame = False
        for player in gcgPlayers:
            print(str(player))
            print(str(ctx.author))
            if str(player) == str(ctx.author):
                inGame = True
                break
            
        if not inGame:
            await ctx.send('Only someone in the current game can start the rolls.')
            return

        # Start the rolling
        rolls = {}
        for player in gcgPlayers:
            rolls[str(player)] = random.randint(1, gcgGameAmount)

        (winner, loser) = getGcgWinner(rolls)
        amount = rolls[winner] - rolls[loser]

        rollString = ''
        for k,v in rolls.items():
            rollString = rollString + str(k) + ' rolled a ' + str(v) + '\n'

        rollString = rollString + '\n'
        rollString = rollString + str(loser) + ' lost ' + str(amount) + currencyString + ' to ' + str(winner)
        
        await ctx.send('-\n*** ROLL RESULTS ***\nPlaying for '+str(gcgGameAmount)+currencyString+'\n\n'+rollString)

        addMoney(winner, amount)
        subtractMoney(loser, amount)

        currentlyPlayingGcg = False
        gcgPlayers = []
        gcgGameAmount = 0

@bot.command()
async def gcgplayers(ctx):
    """ List the players in the current GCG game """
    global currentlyPlayingGcg
    global gcgPlayers
    if not currentlyPlayingGcg:
        await ctx.send('We are not currently gambling.')
    else:
        players = ''
        for player in gcgPlayers:
            players = players + ' ' + str(player)

        await ctx.send('Current players: ' + players)

@bot.command()
async def juice(ctx, user):
    """Checks to see how much juice a user has. Accepts 'me' as a user."""
    global users
    global currencyString

    if user == 'me':
        user = str(ctx.author)
    if user == 'all':
        juicestr = ''
        for k,v in users.items():
            juicestr += str(k) + ': ' + str(v.money)+currencyString+'\n'
        await ctx.send('-\n'+juicestr)
        return
    if user in users:
        await ctx.send(str(user) + ' has ' + str(users[user].money) + currencyString + '.')
    else:
        await ctx.send('I don\'t know who ' + str(user) + ' is.')
        

        
@bot.command()
async def gendercheck(ctx, gender: str):
    """Checks to see if the specified gender is real."""
    try:
        if gender in genderList:
            await ctx.send('Yes, '+gender+' is a real gender.')
        else:
            await ctx.send('No, '+gender+' is not a real gender.')
    except Exception:
        return

@bot.command()
async def trivia(ctx):
    """ Starts a trivia game."""
    global currentlyPlayingTrivia
    global triviaAnswers
    currentlyPlayingTrivia = True
    triviaReward = number = random.randint(1, 8)

    question = getTriviaQuestion()
    if question.difficulty == 'medium':
        triviaReward = triviaReward * 2
    elif question.difficulty == 'hard':
        triviaReward = triviaReward * 3

    triviaString = '-\n**Welcome to the Lemon**\n\n**Reward**: ' + str(triviaReward)+currencyString+'\n**Difficulty**: '+question.difficulty.capitalize()+'\n'+question.category
    
    await ctx.send(str(triviaString))
    await asyncio.sleep(3)
    triviaString = '-\n**Question**\n'+question.question+'\n\n'
    for letter,choice in question.choices.items():
        triviaString += str(letter) +': '+str(choice)+'\n'
    await ctx.send(triviaString)
    await asyncio.sleep(10)
    await ctx.send('Times up! The answer was: ' + str(question.answer.capitalize()))
    for key,answer in triviaAnswers.items():
        if str(answer) == str(question.answer):
            addMoney(str(key.name)+'#'+str(key.discriminator), triviaReward)
    triviaAnswers = {}
    currentlyPlayingTrivia = False

@bot.command()
async def answer(ctx, answer: str):
    """ Answers a trivia question """
    global triviaAnswers
    global currentlyPlayingTrivia

    if currentlyPlayingTrivia:
        player = ctx.author
        if ctx.author not in triviaAnswers:
            triviaAnswers[ctx.author] = answer.lower()
    else:
        await ctx.send('We are not currently playing trivia.')


bot.run('Mjg3MDA0NjQ3NzU0MDM5MzA2.XkCg6A.eliryAFZ0CUfsExUavckb6DCWPY')